;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Program analysis and linting.
;;;
;;; Copyright (c) 2018-2019, Evan Hanson
;;;
;;; See LICENSE for details.
;;;

(declare
  (module (beaker)))

(define (configure-compiler-extension)
  (import (chicken blob)
          (chicken compiler user-pass)
          (chicken eval)
          (chicken format)
          (chicken internal)
          (chicken memory representation)
          (chicken plist)
          (chicken port)
          (chicken pretty-print)
          (chicken sort)
          (chicken string)
          (chicken syntax)
          (srfi 1)
          (srfi 13))

  (define imported-modules (make-vector 32 '()))
  (define referenced-modules (make-vector 32 '()))
  (define global-identifiers (make-vector 32 '()))
  (define warnings (make-vector 128 '()))
  (define deferred-analysers '())
  (define line-number-database (make-vector 1024 '()))

  (define for-each* (flip for-each))

  (define (warn type expr #!optional (loc (get-line-number expr)))
    (unless (not loc)
      (let ((item (cons type expr)))
        (hash-table-update!
         warnings
         (string->symbol loc)
         (lambda (warnings*)
           (alist-cons type expr warnings*))
         (lambda () (list))))))

  (define (defer-analyser thunk)
    (set! deferred-analysers (cons thunk deferred-analysers)))

  (define (hash-table-keys table #!optional (comparator symbol<?))
    (let ((keys '()))
      (hash-table-for-each (lambda (k _) (set! keys (cons k keys))) table)
      (sort keys comparator)))

  (define (symbol<? x y)
    (string<? (symbol->string x) (symbol->string y)))

  (define (location<? x y)
    (let ((x* (string-split (symbol->string x) ":"))
          (y* (string-split (symbol->string y) ":")))
      (and (string<=? (car x*) (car y*))
           (< (string->number (cadr x*))
              (string->number (cadr y*))))))

  (define (pp-length x)
    (string-length (pp-string x)))

  (define (pp-string x)
    (with-output-to-string (lambda () (pp x))))

  (define (pp-fragment x #!optional (w 64) (d 5))
    (let loop ((x x) (w w) (d d))
      (cond ((memq x '(quote quasiquote unquote unquote-splicing)) x)
            ((string? x) x)
            ((symbol? x)
             (string->symbol (loop (format "~s" x) w d)))
            ((vector? x)
             (list->vector (loop (vector->list x) (sub1 w) d)))
            ((atom? x) x)
            ((or (negative? w) (negative? d))
             (list '...))
            (else
             (let* ((x*  (loop (car x) (sub1 w) (sub1 d)))
                    (x** (loop (cdr x) (- (sub1 w) (pp-length x*)) d)))
               (cons x* x**))))))

  (define (expression-contains? x pred)
    (cond ((pred x) #t)
          ((vector? x)
           (expression-contains? (vector->list x) pred))
          ((pair? x)
           (or (expression-contains? (car x) pred)
               (expression-contains? (cdr x) pred)))
          (else #f)))

  (define (expression-contains-identifier? x id)
    (expression-contains? x (lambda (x*) (eq? x* id))))

  (define (constant? x)
    (or (blob? x)
        (boolean? x)
        (char? x)
        (eof-object? x)
        (number? x)
        (string? x)
        (vector? x)
        (##sys#srfi-4-vector? x)))

  (define (quoted-constant? x)
    (and (pair? x)
         (or (compiler-compare (car x) 'scheme 'quote)
             (compiler-compare (car x) 'scheme 'quasiquote))
         (or (constant? (cadr x))
             (quoted-constant? (cadr x)))))

  (define (symbol-namespace sym)
    (and-let* ((str (symbol->string sym))
               (sep (string-index str #\#)))
      (string->symbol (substring str 0 sep))))

  (define (collect-referenced-values db)
    ((flip hash-table-for-each)
     db
     (lambda (sym props)
       (and-let* ((mod (symbol-namespace sym)))
         (hash-table-set! referenced-modules mod 'value)))))

  (define (collect-referenced-syntax x)
    (for-each*
     (get (car x) '##core#db '())
     (lambda (info)
       (when (eq? (car info) 'syntax)
         (hash-table-set! referenced-modules (cadr info) 'syntax)))))

  (define (analyse-import x)
    (for-each*
     (cdr x)
     (lambda (i)
       (receive (m . _) (##sys#decompose-import i values eq? 'import)
         (hash-table-set! imported-modules
                          m
                          (or (get-line-number i)
                              (get-line-number x)))))))

  (define (analyse-if x)
    (define test (cadr x))
    (define arm1 (caddr x))
    (define arm2 (and (pair? (cdddr x)) (cadddr x)))
    (when (not arm2)
      (warn 'one-armed-if x))
    (when (or (and (pair? arm1)
                   (compiler-compare (car arm1) 'scheme 'begin))
              (and (pair? arm2)
                   (compiler-compare (car arm2) 'scheme 'begin)))
      (warn 'if-begin x)))

  (define (analyse-cond x)
    (unless (assq 'else (cdr x))
      (warn 'missing-else x)))

  (define (analyse-case x)
    (unless (assq 'else (cddr x))
      (warn 'missing-else x)))

  (define (analyse-quote x)
    (when (quoted-constant? x)
      (warn 'unnecessary-quote x)))

  (define (analyse-quasiquote x)
    (unless (expression-contains?
             x
             (lambda (x*)
               (and (pair? x*)
                    (or (eq? (car x*) 'unquote)
                        (eq? (car x*) 'unquote-splicing)))))
      (warn 'unnecessary-quasiquote x)))

  (define (named-let? x)
    (and (compiler-compare (car x) 'scheme 'let)
         (symbol? (cadr x))))

  (define (analyse-let x #!optional (type 'let))
    (define bindings (if (named-let? x) (caddr x) (cadr x)))
    (define body     (if (named-let? x) (cdddr x) (cddr x)))
    ;; warn about unused name variable
    (when (named-let? x)
      (unless (expression-contains-identifier? body (cadr x))
        (warn 'unnecessary-named-let x)))
    ;; warn about unnecessary nesting
    (unless (named-let? x)
      (let loop ((body body))
        ;; check for (let () (let () ...))
        (when (and (pair? (car body)))
          (cond ((compiler-compare (caar body) 'scheme type)
                 ;; skip (let () (let foo () ...))
                 (unless (named-let? (car body))
                   (warn (symbol-append 'nested- type) x)))
                ((compiler-compare (caar body) 'scheme 'begin)
                 (loop (cdar body)))
                (else (void))))))
    ;; check for unused bindings
    (let loop ((bindings bindings))
      (unless (null? bindings)
        (unless (or (expression-contains-identifier? body (caar bindings))
                    (and (eq? type 'let*)
                         (expression-contains-identifier? (map cdr (cdr bindings)) (caar bindings))))
          (warn (symbol-append 'unused- type '-binding) x (get-line-number (car bindings))))
        (loop (cdr bindings)))))

  (define (analyse-let* x)
    (analyse-let x 'let*)
    ;; check for unnecessary let* (that could be a normal let)
    (let loop ((vars (map car (cadr x)))
               (vals (map cdr (cadr x))))
      (cond ((null? vars) (warn 'unnecessary-let* x))
            ((expression-contains-identifier? vals (car vars)))
            (else (loop (cdr vars) (cdr vals))))))

  (define (canonicalise-define x)
    (if (symbol? (cadr x))
        x
        (chicken.syntax#expand-curried-define (cadr x) (cddr x) '())))

  (define (analyse-define x)
    (hash-table-set! global-identifiers (cadr (canonicalise-define x)) #t))

  (define (analyse-set! x)
    (let ((var (cadr x))
          (loc (get-line-number x)))
      (when (symbol? var)
        (unless (compiler-ref var)
          (defer-analyser
           (lambda ()
             (unless (hash-table-ref global-identifiers var)
               (warn 'unbound-set! x loc))))))))

  (define (environment-ref env sym)
    (let ((x (assq sym env)))
      (and (pair? x)
           (if (atom? (cdr x))
               (cdr x)
               (caddr x)))))

  (define (module-ref mod sym)
    (environment-ref (block-ref (module-environment mod) 2) sym))

  (define (compiler-ref sym)
    (or (environment-ref (##sys#current-environment) sym)
        (environment-ref (##sys#macro-environment) sym)))

  (define (compiler-compare sym mod id)
    (eq? (compiler-ref sym) (module-ref mod id)))

  (define (analyse-expression x)
    (when (and (pair? x) (symbol? (car x)))
      (cond ((eq? (car x) 'import) (analyse-import x))
            ((compiler-compare (car x) 'scheme 'case) (analyse-case x))
            ((compiler-compare (car x) 'scheme 'cond) (analyse-cond x))
            ((compiler-compare (car x) 'scheme 'define) (analyse-define x))
            ((compiler-compare (car x) 'scheme 'if) (analyse-if x))
            ((compiler-compare (car x) 'scheme 'let) (analyse-let x))
            ((compiler-compare (car x) 'scheme 'let*) (analyse-let* x))
            ((compiler-compare (car x) 'scheme 'quasiquote) (analyse-quasiquote x))
            ((compiler-compare (car x) 'scheme 'quote) (analyse-quote x))
            ((compiler-compare (car x) 'scheme 'set!) (analyse-set! x))
            (else (collect-referenced-syntax x)))))

  (define (analyse-imports)
    (let* ((imported   (hash-table-keys imported-modules))
           (referenced (hash-table-keys referenced-modules))
           (difference (lset-difference eq? imported referenced)))
      (for-each (lambda (m)
                  (warn 'unnecessary-import (list m) (hash-table-ref imported-modules m)))
                difference)))

  (define (emit severity table)
    (for-each*
     (hash-table-keys table location<?)
     (lambda (loc)
       (for-each*
        (hash-table-ref table loc)
        (lambda (warning)
          (parameterize ((pretty-print-width 1024))
            (pretty-print
             (cons* (symbol->string loc) severity (car warning)
                    (pp-fragment (list (cdr warning)))))))))))

  (define (print-analysis-results)
    (parameterize ((current-output-port (current-error-port)))
      (emit 'warning warnings)))

  (set! ##sys#compiler-syntax-hook
    (let ((next ##sys#compiler-syntax-hook))
      (lambda (name x)
        (and-let* ((mod (symbol-namespace name)))
          (hash-table-set! referenced-modules mod 'compiler-syntax))
        (next name x))))

  (set! ##sys#expand-0
    (let ((next ##sys#expand-0))
      (lambda (x e c)
        ;; NOTE we expand the form first so that the compiler's
        ;; syntax checks run and we can assume the form is valid
        (receive (x* m) (next x e c)
          (fluid-let ((##sys#line-number-database line-number-database))
            (analyse-expression x)
            (values x* m))))))

  (user-preprocessor-pass
   (let ((done #f)
         (next (or (user-preprocessor-pass) values)))
     (lambda (x)
       (when (and (not done) ##sys#line-number-database)
         (set! done #t)
         (hash-table-for-each (lambda (k v)
                                (hash-table-set! line-number-database k v))
                              ##sys#line-number-database))
       (next x))))

  (user-pass
   (let ((next (or (user-pass) values)))
     (lambda (x)
       (let ((analysers deferred-analysers))
         (set! deferred-analysers '())
         (for-each (lambda (f) (f)) analysers))
       (next x))))

  (user-post-analysis-pass
   (let ((done #f)
         (next (or (user-post-analysis-pass) void)))
     (lambda (pass db node get set count continue)
       (next pass db node get set count continue)
       (when (and (not done)
                  (or (eq? pass 'scrutiny)
                      (and (eq? pass 'opt) (= count 1))))
         (set! done #t)
         (collect-referenced-values db)
         (analyse-imports)
         (print-analysis-results))))))

(let ()
  (import (chicken platform))
  (when (feature? #:compiler-extension)
    (configure-compiler-extension)))
