;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Helper procedures for working with egg files.
;;;
;;; Copyright (c) 2018-2019, Evan Hanson
;;;
;;; See LICENSE for details.
;;;

(declare
  (module (beaker egg info))
  (export egg-author
          egg-dependencies
          egg-extensions
          egg-import-libraries
          egg-name
          egg-programs)
  (import (chicken condition)
          (chicken pathname)
          (srfi 1)))

(define (egg-name egg-file)
  (string->symbol (pathname-file egg-file)))

(define (egg-info-slot egg-info name)
  (alist-ref name egg-info eq? '()))

(define (read-egg-info egg-file)
  (handle-exceptions _ '() (with-input-from-file egg-file read)))

(define (egg-dependencies egg-file #!optional (build #t) (test #f))
  (let ((egg-info (read-egg-info egg-file)))
    (map (lambda (d) (if (pair? d) (car d) d))
         (append (egg-info-slot egg-info (and build 'build-dependencies))
                 (egg-info-slot egg-info 'dependencies)
                 (egg-info-slot egg-info (and test 'test-dependencies))))))

(define (egg-author egg-file)
  (let ((author (egg-info-slot (read-egg-info egg-file) 'author)))
    (and (pair? author) (car author))))

(define (egg-components egg-file #!optional type)
  (let ((egg-info (read-egg-info egg-file)))
    (filter-map (lambda (c)
                  (and (pair? c)
                       (pair? (cdr c))
                       (symbol? (cadr c))
                       (or (not type)
                           (eq? (car c) type))
                       (cdr c)))
                (egg-info-slot egg-info 'components))))

(define (egg-extensions egg-file)
  (map car (egg-components egg-file 'extension)))

(define (egg-import-libraries egg-file)
  (append-map (lambda (x)
                 (or (alist-ref 'modules (cdr x))
                     (list (car x))))
              (egg-components egg-file 'extension)))

(define (egg-programs egg-file)
  (map car (egg-components egg-file 'program)))
