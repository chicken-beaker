;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Helper procedures for command input/output.
;;;
;;; Copyright (c) 2018-2022, Evan Hanson
;;;
;;; See LICENSE for details.
;;;

(declare
  (module (beaker interactive))
  (export interactive verbosity message prompt)
  (import (chicken io) (srfi 13)))

(define verbosity (make-parameter 1))
(define interactive (make-parameter #f))

(define (prompt message #!optional (in (current-input-port)) (out (current-output-port)))
  (or (not (interactive))
      (let loop ((response #f))
        (if (not response)
            (display (string-append message " (yes/no/abort): ") out)
            (display "Please enter one of (yes/no/abort): " out))
        (flush-output out)
        (let ((r (string-trim-both (read-line in))))
          (cond ((string=? r "yes") #t)
                ((string=? r "no") #f)
                ((string=? r "abort") (exit 1))
                (else (loop r)))))))

(define (message level text #!optional (out (current-output-port)))
  (unless (< (verbosity) level)
    (display text out)
    (newline out)))
