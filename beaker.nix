let
  eggsHash = "sha256-ar8gJaszIC+XbnUbJS6IsvcHMIRSTmF3jBXlvF3XC4U=";
in {
  pkgs ? import <nixpkgs> {},
  beaker ? import ./default.nix { inherit pkgs; }
}:
with beaker;
let
  eggs = eggCache { hash = eggsHash; eggs = ./beaker.egg.lock; };
  repo = eggRepository { src = eggs; };
in eggProgram {
  name = "beaker";
  src = ./.;
  buildInputs = [ repo ];
}
