(import (srfi 1)) ;> ("examples.scm:1" warning unnecessary-import)
(let () (import (srfi 13))) ;> ("examples.scm:2" warning unnecessary-import)
(let () (import (srfi 14)) char-set)

(let ((x 1)) (void)) ;> ("examples.scm:5" warning unused-let-binding)
(let ((x 1)) (void x))
(let ((x 1) (y x)) (void y)) ;> ("examples.scm:7" warning unused-let-binding)
(let ((x 1) (y x)) (void x)) ;> ("examples.scm:8" warning unused-let-binding)
(let ((x 1) (y x)) (void x y))

(let* ((x 1) (y x)) (void x)) ;> ("examples.scm:11" warning unused-let*-binding)
(let* ((x 1) (y x)) (void y))

(let ((x 1)) (let loop () (loop))) ;> ("examples.scm:14" warning unused-let-binding)
(let ((x 1)) (let loop ((y 2)) (loop y))) ;> ("examples.scm:15" warning unused-let-binding)
(let ((x 1)) (let loop ((y x)) (loop y)))

(let loop ((x 1)) (loop 1)) ;> ("examples.scm:18" warning unused-let-binding)
(let loop ((x 1)) (loop x))

(let* ((x 1)) (void x)) ;> ("examples.scm:21" warning unnecessary-let*)
(let* ((x 1) (y 2)) (void x y)) ;> ("examples.scm:22" warning unnecessary-let*)
(let* ((x 1) (y x)) (void x y))

(let loop () (void)) ;> ("examples.scm:25" warning unnecessary-named-let)
(let loop () (loop))

(let ((x x)) (let ((y y)) (void x y))) ;> ("examples.scm:28" warning nested-let)
(let ((x x)) (let* ((y y)) (void x y)))

(let* ((x x)) (let* ((y y)) (void x y))) ;> ("examples.scm:31" warning nested-let*)
(let* ((x x)) (let ((y y)) (void x y)))

;; https://todo.sr.ht/~evhan/beaker/5
(module foo ())

(quote #t) ;> ("examples.scm:37" warning unnecessary-quote)
(quote "foo") ;> ("examples.scm:38" warning unnecessary-quote)
(quote (quote #t)) ;> ("examples.scm:39" warning unnecessary-quote)
(quote (quasiquote #t)) ;> ("examples.scm:40" warning unnecessary-quote)
(quote foo)
(quote (quote foo))

(if x (begin y y) z) ;> ("examples.scm:44" warning if-begin)
(if x y (begin z z)) ;> ("examples.scm:45" warning if-begin)
