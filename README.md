# Beaker

Lab supplies for CHICKEN Scheme.

## Description

Beaker is a collection of development tools.

It is currently fairly limited, containing only a few programs, a handful of
extension libraries, and Nix helpers to make common development tasks easier.
If you have an idea for something that would be useful to include, don't
hesitate to contact the author.

## Documentation

Documentation can be found on the [CHICKEN wiki][wiki].

[wiki]: https://wiki.call-cc.org/eggref/5/beaker

## Contributing

Please report issues [here](https://todo.sr.ht/~evhan/beaker).

Patches can be mailed to the author.

## Author

Evan Hanson <evhan@foldling.org>

## License

Beaker is licensed under the [3-clause BSD license][license].

[license]: https://opensource.org/licenses/BSD-3-Clause
