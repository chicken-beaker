{
  outputs = { self, nixpkgs }:
    let
      systems = [ "aarch64-linux" "aarch64-darwin" "x86_64-darwin" "x86_64-linux" ];
      forAllSystems = nixpkgs.lib.genAttrs systems;
      callForSystem = system: nixpkgs.legacyPackages.${system}.callPackage ./. {};
      eggHelper = name: system: args: (callForSystem system).${name} args;
    in {
      lib = forAllSystems callForSystem // {
        eggCache = { system, ... } @ args: eggHelper "eggCache" system args;
        eggDerivation = { system, ... } @ args: eggHelper "eggDerivation" system args;
        eggProgram = { system, ... } @ args: eggHelper "eggProgram" system args;
      };

      packages = forAllSystems (system: rec {
        beaker = (callForSystem system).beaker;
        default = beaker;
      });
    };
}
