;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Lint Scheme source files.
;;;
;;; Copyright (c) 2018-2022, Evan Hanson
;;;
;;; See LICENSE for details.
;;;

;;
;; The `chicken-lint` program checks a source file with a set of simple
;; lint rules.
;;
;; Potential problems are written as S-expressions to standard error.
;;
;; Note that this program invokes `csc`, so any compile-time code in the
;; program will be executed.
;;
(declare
  (module (chicken-lint))
  (import (chicken foreign)
          (chicken format)
          (chicken pathname)
          (chicken process)
          (chicken process-context)))

(define (usage status)
  (printf "Usage: ~a [csc-options ...] filename ..." (pathname-file (program-name)))
  (print)
  (exit status))

(define (csc)
  (make-pathname
   (foreign-value "C_TARGET_BIN_HOME" c-string)
   (foreign-value "C_CSC_PROGRAM" c-string)))

(define (csc-options)
  (list "-analyze-only" "-extend" "beaker" "-no-warnings" "-optimize-level" "0"))

(define (analyse-program args)
  (receive (_ _ status) (process-wait (process-run (csc) (append (csc-options) args)))
    (exit status)))

(define (main)
  (let ((args '()))
    ((flip for-each)
     (command-line-arguments)
     (lambda (arg)
       (if (member arg '("-h" "-help" "--help"))
           (usage 0)
           (set! args (cons arg args)))))
    (analyse-program (reverse args))))

(cond-expand
  (compiling (main))
  (else))
