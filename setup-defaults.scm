;;;
;;; Generate a setup.defaults file that will make chicken-install fetch
;;; eggs from the local directories given as command line arguments.
;;;

(import (chicken pathname)
        (chicken process-context)
        (chicken io))

(define +install-prefix+ (normalize-pathname (make-pathname (executable-pathname) "../..")))
(define +setup-defaults+ (make-pathname +install-prefix+ "share/chicken/setup.defaults"))

(for-each (lambda (x) (write x) (newline))
          (append (call-with-input-file +setup-defaults+ read-list)
                  (map (lambda (l) (list 'location l)) (command-line-arguments))))
