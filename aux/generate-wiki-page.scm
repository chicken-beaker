#!/bin/sh
#|
exec csi -s "$0" "$@"
|#

(import (beaker markdown))
(import (chicken io))
(import (only (chicken port) make-concatenated-port))
(import (markdown-svnwiki))

(with-output-to-file "beaker.wiki"
 (lambda ()
   (let ((markdown (open-output-string))
         (template (make-concatenated-port
                    (open-input-string "#<#EOF\n")
                    (open-input-file "beaker.wiki.in")
                    (open-input-string "EOF\n"))))
     (display (eval (read template)) markdown)
     (markdown->svnwiki (get-output-string markdown)))))
